#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <string>
#include <unordered_set>

int main()
{
    std::ifstream input_file("input2");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::list<std::string> id_list;
    for( std::string line; std::getline( input_file, line ); ) {
        id_list.push_back(line);
    }
    input_file.close();

    std::string result;
    for(auto item_i = id_list.begin(); item_i != id_list.end(); item_i++) {
        std::cout << *item_i;
        for(auto item_j = id_list.begin(); item_j != id_list.end(); item_j++) {
            bool differ_1 = false;
            unsigned int differ_pos = 0;
            for (unsigned int i = 0; i < item_i->size(); i++ ) {
                if (item_i->at(i) != item_j->at(i)) {
                    if (differ_1) {
                        differ_1 = false;
                        break;
                    }
                    differ_1 = true;
                    differ_pos = i;
                }
            }
            if (differ_1) {
                std::copy(item_i->begin(), item_i->end(), std::back_inserter(result));
                result.replace(differ_pos, differ_pos + 1, "");
                std::cout <<  "  " << item_i->at(differ_pos) << item_j->at(differ_pos);
            }
        }
        std::cout << std::endl;
    }
    std::cout << "result is " << result << std::endl;
    return 0;
}
