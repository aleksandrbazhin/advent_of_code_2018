#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <algorithm>
#include <string>
#include <regex>
#include <unordered_map>

struct Point {
    unsigned int x;
    unsigned int y;
    Point(unsigned int x, unsigned int y) : x(x), y(y) {}
};

struct Unit {
    bool is_elf;
    Point pos;
    int hp = 200;
    Unit(bool is_elf, unsigned int x, unsigned int y) :
        is_elf(is_elf), pos(Point(x, y)) {}
};

int main()
{
    std::ifstream input_file("input15");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::vector<std::vector<int>> map;
    std::vector<Unit> units;

    unsigned int i = 0;
    for( std::string line; std::getline( input_file, line ); ) {
        std::vector<int> row;
        for (unsigned int j = 0; j < line.size(); j++) {
            if (line[j] == '#') {
                row.push_back(0);
            } else {
                row.push_back(1);
                if (line[j] == 'E') {
                    units.push_back(Unit(true, i, j));
                } else if (line[j] == 'G') {
                    units.push_back(Unit(false, i, j));
                }
            }
        }
        i++;
        map.push_back(row);
    }
    input_file.close();

    for (unsigned int i = 0; i < map.size(); i++) {
        for (unsigned int j = 0; j < map[0].size(); j++) {
            std::cout << map[i][j];
        }
        std::cout << std::endl;
    }

    for (Unit u: units) {
        std::cout << (u.is_elf ? "Elf " : "Goblin ")  << u.pos.x << " " << u.pos.y << std::endl;
    }


    return 0;
}
