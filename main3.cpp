#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <map>
#include <string>
#include <regex>

struct Patch {
    unsigned int x;
    unsigned int y;
    unsigned int x_size;
    unsigned int y_size;
    bool intersects;
};

int main()
{
    std::ifstream input_file("input3");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::list<std::string> id_list;
    for( std::string line; std::getline( input_file, line ); ) {
        id_list.push_back(line);
    }
    input_file.close();

    std::regex parse_regex("[[:digit:]]+");
    std::vector<std::vector<unsigned int>> fabric;
    std::map<unsigned int, Patch> patches;
    fabric.resize(1000, std::vector<unsigned int>(1000, 0));
    for(auto item = id_list.begin(); item != id_list.end(); item++) {
        std::cout << *item << "  ";
        auto match_iter = std::sregex_iterator(item->begin(), item->end(), parse_regex);
        Patch patch{ 0, 0, 0, 0, false};
        unsigned int id = std::stoi(match_iter->str());
        match_iter++;
        patch.x = std::stoi(match_iter->str());
        match_iter++;
        patch.y = std::stoi(match_iter->str());
        match_iter++;
        patch.x_size = std::stoi(match_iter->str());
        match_iter++;
        patch.y_size = std::stoi(match_iter->str());
        std::cout << id << "  " << patch.x << "  " << patch.y << "  "
                  << patch.x_size << "  " << patch.y_size << "  ";
        for (unsigned int i = patch.x; i < patch.x + patch.x_size; i++) {
            for (unsigned int j = patch.y; j < patch.y + patch.y_size; j++) {
                if (fabric[i][j] != 0) {
                    (patches[fabric[i][j]]).intersects = true;
                    patch.intersects = true;
                }
                fabric[i][j] = id;
            }
        }
        patches.insert(std::make_pair(id, patch));
        std::cout << std::endl;
    }
    unsigned int good_patch_id = 0;
    for (auto const &patch : patches) {
        if (!patch.second.intersects) {
            good_patch_id = patch.first;
        }
    }

    std::cout << "result is " << good_patch_id << std::endl;
    return 0;
}
