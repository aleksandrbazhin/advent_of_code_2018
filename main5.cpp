#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>


bool fold_line(std::string &line) {
    bool folded = false;
    for (unsigned int i = 1; i < line.size(); i++) {
        int diff = (int)line[i] - (int)line[i-1];
        if (diff == 32 || diff == -32) {
            line[i] = '\0';
            line[i-1] = '\0';
            folded = true;
        }
    }
    return folded;
}

void string_remove(std::string &str, char ch) {
    std::string::iterator end =
            std::remove(str.begin(), str.end(), ch);
    str.erase(end, str.end());
}


int main()
{
    std::ifstream input_file("input5");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::string line;
    std::getline( input_file, line );

    int min_len = line.size();
    for (unsigned int ch = 65; ch < 91; ch++) {
        std::string test_line = line;
        string_remove(test_line, (char)ch);
        string_remove(test_line, (char)(ch + 32));
        bool folds = true;
        while (folds) {
            folds = fold_line(test_line);
            string_remove(test_line, '\0');
        }
        if (min_len > test_line.size()) {
            min_len = test_line.size();
        }
    }
    std::cout << min_len << std::endl;
    return 0;
}
