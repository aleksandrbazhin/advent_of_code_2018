#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <algorithm>
#include <string>
#include <regex>
#include <unordered_map>

struct Shift {
    std::string date;
    std::vector<std::pair<unsigned int, unsigned int>> sleeps;
};

struct Guard {
    std::vector<Shift> shifts;
    std::vector<unsigned int> sleep_by_minute;
    unsigned int time_asleep = 0;
    Guard () : sleep_by_minute(60) {}
};

int main()
{
    std::ifstream input_file("input4");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::list<std::string> string_list;
    for( std::string line; std::getline( input_file, line ); ) {
        string_list.push_back(line);
    }
    input_file.close();

    string_list.sort();

    std::unordered_map<unsigned int, Guard> guards;
    std::regex begin_regex("\\[(.+)\\] Guard #([[:digit:]]+) begins shift");
    std::regex fall_regex("\\[.+([[:digit:]]{2})\\] falls asleep");
    std::regex wake_regex("\\[.+([[:digit:]]{2})\\] wakes up");
    unsigned int guard_id = 0;
    for (auto const &str : string_list) {
        std::cout << str;

        std::smatch id_match, fall_match, wake_match;
        if (std::regex_search(str, id_match, begin_regex)) {
            guard_id = std::stoi(id_match[2]);
            if (guards.count(guard_id) == 0) {
                Guard new_guard;
                guards.insert({guard_id, new_guard});
            }
            Shift new_shift;
            new_shift.date = id_match[1];
            guards.at(guard_id).shifts.push_back(new_shift);
        } else if (std::regex_search(str, fall_match, fall_regex)) {
            unsigned int minutes = std::stoi(fall_match[1]);
            guards.at(guard_id).shifts.back().sleeps.push_back({minutes, 59});
        } else if (std::regex_search(str, wake_match, wake_regex)) {
            unsigned int minutes = std::stoi(wake_match[1]);
            guards.at(guard_id).shifts.back().sleeps.back().second = minutes;
        }
        std::cout << std::endl;
    }

    for (auto &guard : guards) {
        for (auto const &shift : guard.second.shifts) {
            for (auto const &sleep : shift.sleeps) {
                for (unsigned int i = sleep.first; i < sleep.second; i++) {
                    guard.second.sleep_by_minute[i] ++;
                }
            }
        }
    }

    unsigned int searched_min = 0,
            searched_guard_id = 0,
            max_sleep_in_min = 0;
    for (auto const &guard : guards) {
        for (unsigned int i = 0; i < 60; i++) {
            unsigned int sleep_in_min = guard.second.sleep_by_minute[i];
            if (sleep_in_min > max_sleep_in_min) {
                max_sleep_in_min = sleep_in_min;
                searched_guard_id = guard.first;
                searched_min = i;
            }
        }
    }

    std::cout << "result is " << searched_guard_id << " * " << searched_min
              << " = " << searched_guard_id * searched_min << std::endl;
    return 0;
}
