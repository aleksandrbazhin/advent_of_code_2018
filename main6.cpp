#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>


int main()
{
    std::ifstream input_file("input5");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::string line;
    std::getline( input_file, line );

    return 0;
}
