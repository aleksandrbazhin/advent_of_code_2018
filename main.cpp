#include <iostream>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_set>

int main(int argc, char *argv[])
{
    std::ifstream input_file("input");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::vector<long> freq_vec;
    for( std::string line; std::getline( input_file, line ); ) {
        if (line.size() > 0) {
            std::istringstream line_stream(line);
            long acc = 0;
            line_stream >> acc;
            freq_vec.push_back(acc);
        }
    }
    input_file.close();

    long sum_freq = 0;
    bool duplicate_found = false;
    std::unordered_set<long> freq_history;
    unsigned int i = 0;
    while (!duplicate_found) {
        auto inserted = freq_history.emplace(sum_freq);
        if (!inserted.second) {
            duplicate_found = true;
            break;
        }
        std::cout << "#" << i << ": " << sum_freq << " + " << freq_vec[i] << " = ";
        sum_freq += freq_vec[i];
        std::cout << sum_freq << std::endl;
        i++;
        if (i >= freq_vec.size()) {
            i = 0;
        }
    }
    std::cout << "sum is " << sum_freq << std::endl;
    return 0;
}
